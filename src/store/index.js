import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    Users: [
      { id: 1, name: "User Alex", posts: "12"},
      { id: 2, name: "User Lao", posts: "2"},
      { id: 3, name: "User Mew", posts: "12"},
      { id: 4, name: "User Klo", posts: "23"}
    ],
    Posts: [
      { id: 1, title: "Post Alex", description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).", user: 'User 1'},
      { id: 2, title: "Post Lao", description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).", user: 'User 6'},
      { id: 3, title: "Post Mew", description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).", user: 'User 2'},
      { id: 4, title: "Post Klo", description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).", user: 'User 5'}
    ],
    Reports: [
      { id: 1, name: "Report post 1", posts: "12"},
      { id: 2, name: "Report post 2", posts: "2"},
      { id: 3, name: "Report post 3", posts: "12"},
      { id: 4, name: "Report post 3", posts: "23"}
    ]
  },
  mutations: {

  },
  actions: {

  },
  getters: {
    initUsers(state) {
      return state.Users
    },
    initPosts(state) {
      return state.Posts
    },
    initReports(state) {
      return state.Reports
    },

  }
});