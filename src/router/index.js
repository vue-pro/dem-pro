import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home'
import User from '@/components/User'
import Users from '@/components/Users'
import Post from '@/components/Post'
import Report from '@/components/Report'
import Posts from '@/components/Posts'
import Admin from '@/components/Admin'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/user/:id',
      name: 'User',
      component: User
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    },
    {
      path: '/post/:id',
      name: 'Post',
      component: Post
    },
    {
      path: '/report/:id',
      name: 'Report',
      component: Report
    },
    {
      path: '/posts',
      name: 'Posts',
      component: Posts
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin
    },
    { path: "/*",
      redirect: { name: "Home" },
    },
  ]
})
